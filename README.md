# Tezos Bake Monitor Lib

This is a haskell library for interacting with a Tezos RPC. It is currently used by http://gitlab.com/obsidian.systems/kiln and a few other Obsidian Tezos projects. A better name for this libary is on our TODO list!

The api is not guaranteed to be stable nor is it very well documented for public consumption, but it is MIT licensed so please feel free to use it if it is useful to you! 

We are currently thinking about what this library should look like if we start supporting it as a library that is friendly to all Haskell Tezos developers, so if you have feedback, please feel free to drop us an issue!

## The library comes in two parts

- tezos-bake-monitor-lib: All of the JSON serialisation types and code that is GHCJs friendly.
- tezos-noderpc: The code that will actually do HTTP calls to a tezos node.

If you only have backend code, you'll just need to use noderpc. If you have a GHCJs frontend and want to send tezos types to the frontend, then import tbml into your frontend.
