let 
  obelisk = import ../dep/obelisk {};
  addBuildTools = obelisk.nixpkgs.haskell.lib.addBuildTools;
in (addBuildTools ((import ../. {}).tezos-bake-monitor-lib) (with (obelisk.obelisk); [cabal-install ghcid])).env
