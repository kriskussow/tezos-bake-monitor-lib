module Tezos.V004.Unsafe (module X) where

import Tezos.V004.ProtocolConstants as X (unsafeAssumptionLevelToCycle, unsafeAssumptionFirstLevelInCycle, unsafeAssumptionRightsContextLevel)
