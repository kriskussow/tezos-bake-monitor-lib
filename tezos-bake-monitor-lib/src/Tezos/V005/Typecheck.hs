{-# LANGUAGE TemplateHaskell #-}

module Tezos.V005.Typecheck where

import Control.Lens
import Data.Typeable

import Tezos.Common.Json
import Tezos.V005.Micheline

data TypecheckData = TypecheckData
  { _typecheckData_data :: !Expression  -- "data": $micheline.michelson_v1.expression
  , _typecheckData_type :: !Expression -- "type": $micheline.michelson_v1.expression
  , _typecheckData_gas :: !TezosBigNum -- "gas"?: $bignum
  }
  deriving (Eq, Ord, Show, Typeable)

data TypecheckDataResult = TypecheckDataResult
  { _typecheckDataResult_gas :: !TezosBigNum -- "gas": $bignum || "unaccounted"
  }
  deriving (Eq, Ord, Show, Typeable)

concat <$> traverse makeLenses
  [ ''TypecheckData
  , ''TypecheckDataResult
  ]

concat <$> traverse deriveTezosJson
  [ ''TypecheckData
  , ''TypecheckDataResult
  ]
