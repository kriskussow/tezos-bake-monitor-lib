{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
module Tezos.V005.Account
  ( module Tezos.V005.Account
  , module Old
  )
  where

import Control.Lens.TH (makeLenses)
import Data.Typeable

import Tezos.Common.Json
import Tezos.V005.Contract
import Tezos.V005.PublicKeyHash
import Tezos.V005.PublicKey
import Tezos.V005.Tez

import Tezos.V004.Account as Old hiding (Account(..), account_delegate, account_balance, account_counter, account_script, ManagerKey(..), managerKey_manager, managerKey_key)

{-
Changes between V005 and V005:
  - Counter is now optional
  - spendable and manager fields are removed
  - delegate.delegatable are removed, meaning delegate is now just an optional PKH vs a mandatory sub record

Also, this depends on new ContractScript because there is a new Micheline primitive.

See https://tezos.gitlab.io/master/protocols/005_babylon.html for further details.
-}

data Account = Account
  { _account_delegate :: !(Maybe PublicKeyHash) --  { "$ref": "#/definitions/Signature.Public_key_hash" },
  , _account_balance :: !Tez -- "2052452947621" "balance": { "$ref": "#/definitions/mutez" },
  , _account_script :: !(Maybe ContractScript) -- "script": { "$ref": "#/definitions/scripted.contracts" },
  , _account_counter :: !(Maybe Counter)  -- "counter": { "$ref": "#/definitions/positive_bignum" }
  } deriving (Show, Eq, Ord, Typeable)

type ManagerKey = Maybe PublicKey

concat <$> traverse deriveTezosJson
  [ ''Account
  ]

concat <$> traverse makeLenses
  [ 'Account
  ]
