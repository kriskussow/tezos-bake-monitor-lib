# This allows us to CI multiple obelisks if we need to. Right now, we just default to kilns
# obelisk and overrides.
{ obelisk ? (import dep/obelisk {}) 
, overrides ? null 
}:
let
  hackGet = obelisk.reflex-platform.hackGet;
  composeExtensions = obelisk.nixpkgs.lib.composeExtensions;
  hasPrefix = obelisk.nixpkgs.lib.hasPrefix;
  dontCheck = obelisk.nixpkgs.haskell.lib.dontCheck;
  purifyEnvironment =
    let isImpure = p: hasPrefix ".ghc.environment" p || builtins.elem p [".git" "result" "dist-newstyle"];
    in builtins.filterSource (path: type: !isImpure (baseNameOf path));
  
  defaultOverrides = self: super: 
    {
      micro-ecc = self.callCabal2nix "micro-ecc" (hackGet ./dep/micro-ecc-haskell) {};
    };

  actualOverrides = if overrides == null then defaultOverrides else overrides;

  ghc = obelisk.reflex-platform.ghc.override
  {
    overrides = composeExtensions actualOverrides (self: super:
      {
        tezos-bake-monitor-lib = self.callCabal2nix "tezos-bake-monitor-lib" (purifyEnvironment ./tezos-bake-monitor-lib) {};
        tezos-noderpc = self.callCabal2nix "tezos-noderpc" (purifyEnvironment ./tezos-noderpc) {};
      });
  };
  ghcjs = obelisk.reflex-platform.ghcjs.override
  {
    overrides = composeExtensions actualOverrides (self: super:
      {
        tezos-bake-monitor-lib = self.callCabal2nix "tezos-bake-monitor-lib" ./tezos-bake-monitor-lib {};
        base58-bytestring = dontCheck super.base58-bytestring; # disable tests for GHCJS build
        email-validate = dontCheck super.email-validate; # disable tests for GHCJS build
      });
  };
in {
  # This is a bit ugly, but really all it is doing is what we'd normally do in a release.nix anyway.
  # No one actuall cares about the nix-built artifact here as they'll callCabal2nix things in themselves
  # into their own obelisk environment.
  inherit (ghc) tezos-bake-monitor-lib tezos-noderpc;
  tezos-bake-monitor-lib-ghcjs = ghcjs.tezos-bake-monitor-lib;
}
